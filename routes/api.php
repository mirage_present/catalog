<?php

use App\Http\Requests\LoginRequest;
use App\Http\Requests\ProductRequest;
use App\Product;
use \App\User;
use \Illuminate\Support\Facades\Auth;

$router->group(['prefix' => '/api/v1'], function () use ($router) {
    // CORS route
    $router->options('/', function () {
        return response('', 200);
    });

    // User routes
    $router->group(['prefix' => '/users'], function () use ($router) {
        $router->post('/login', function (LoginRequest $request) {
            /** @var User $user */
            $user = User::where('username', $request->username)->first();

            if ($user->isCorrectPassword($request->password)) {
                return response()->json(['token' => $user->generateToken()]);
            }

            return response()->json([
                'username' => ['These credentials do not match our records.']
            ], 422);
        });

        // Auth routes
        $router->group(['middleware' => 'auth'], function () use ($router) {
            $router->get('/logout', function () {
                Auth::user()->destroyToken();
            });
            $router->get('/me', function () {
                return Auth::user();
            });
        });
    });

    // Products
    $router->group(['prefix' => '/products', 'middleware' => 'auth'], function () use ($router) {
        $router->post('/', function (ProductRequest $request) {
            /** @var Product $product */
            $product = Product::create(array_merge(
                $request->only(['name', 'price', 'amount']),
                [ Product::FOREIGN_USER => Auth::id() ]
            ));

            return response()->json($product, 201);
        });
        $router->put('/{product}', function (ProductRequest $request, Product $product) {
            $product->update($request->only(['name', 'price', 'amount']));

            return $product;
        });
        $router->get('/{product}', function (Product $product) {
            return $product;
        });
    });
});
