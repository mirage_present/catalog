<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @author Davyd Holovii <mirage.present@gmail.com>
 * @since  30.05.2019
 *
 * @property int       $id
 * @property int       $amount
 * @property int       $created_by
 * @property float     $price
 * @property string    $name
 *
 * @property-read User $creator User relation
 */
class Product extends Model
{
    /**
     * User relation foreign key name
     *
     * @var string
     */
    public const FOREIGN_USER = 'created_by';

    /**
     * Model does not have timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'amount',
        'created_by',
    ];

    /**
     * Belongs to User relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, self::FOREIGN_USER);
    }
}
