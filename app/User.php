<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * Class User
 *
 * @author Davyd Holovii <mirage.present@gmail.com>
 * @since  30.05.2019
 *
 * @property int            $id
 * @property string         $username
 * @property string         $password
 * @property string         $api_token
 *
 * @property-read Product[] $products
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * Model does not have timestamps
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'api_token',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'api_token',
    ];

    /**
     * Has many products relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, Product::FOREIGN_USER);
    }

    /**
     * Check if provided password is correct
     *
     * @param string $password
     *
     * @return bool
     */
    public function isCorrectPassword(string $password): bool
    {
        return app('hash')->check($password, $this->password);
    }

    /**
     * Create and return new api_token
     *
     * @return string
     * @throws \Exception
     */
    public function generateToken(): string
    {
        $this->api_token = bin2hex(random_bytes(32));
        $this->save();

        return $this->api_token;
    }

    public function destroyToken(): void
    {
        $this->api_token = null;
        $this->save();
    }
}
