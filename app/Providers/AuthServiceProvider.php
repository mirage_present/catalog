<?php

namespace App\Providers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Token's header name
     *
     * @var string
     */
    protected const TOKEN_NAME = 'x-access-token';

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function (Request $request) {
            return $request->headers->has(self::TOKEN_NAME)
                ? User::where('api_token', $request->headers->get(self::TOKEN_NAME))->first()
                : null;
        });
    }
}
