<?php

namespace App\Providers;

use App\Product;
use mmghv\LumenRouteBinding\RouteBindingServiceProvider as ServiceProvider;

/**
 * Class RouteServiceProvider
 *
 * @author Davyd Holovii <mirage.present@gmail.com>
 * @since  31.05.2019
 */
class RouteServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->binder->bind('product', Product::class);
    }
}
