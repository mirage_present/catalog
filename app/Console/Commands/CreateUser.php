<?php

namespace App\Console\Commands;

use App\User;
use Faker\Generator;
use Illuminate\Console\Command;

/**
 * Class CreateUser
 *
 * @author Davyd Holovii <mirage.present@gmail.com>
 * @since  31.05.2019
 */
class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "user:create";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Create user for test API";

    /**
     * Command handler
     */
    public function handle(): void
    {
        /** @var string $username */
        $username = $this->getUniqueUsername();
        /** @var string $password */
        $password = bin2hex(random_bytes(rand(4,6)));

        // If user already exists
        if (!$username || User::where('username', $username)->count()) {
            $this->error("Username is not valid or already exists");

            return;
        }

        User::create(compact('username', 'password'));

        $this->info("User has been created!");
        $this->line("Username: {$username}");
        $this->line("Password: {$password}");
    }

    /**
     * Creates unique username
     *
     * @return string
     */
    private function getUniqueUsername(): string
    {
        /** @var Generator $faker */
        $faker = \Faker\Factory::create();

        $username = $faker->userName;

        return User::where('username', $username)->count() ? $this->getUniqueUsername() : $username;
    }
}
