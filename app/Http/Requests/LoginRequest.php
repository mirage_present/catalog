<?php

namespace App\Http\Requests;

/**
 * Class LoginRequest
 *
 * @author Davyd Holovii <mirage.present@gmail.com>
 * @since  30.05.2019
 *
 * @property-read string $username
 * @property-read string $password
 */
class LoginRequest extends JsonRequest
{
    /**
     * Validation rules
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'username' => 'required|max:50|exists:users,username',
            'password' => 'required|min:6|max:30',
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
            'username.exists' => 'These credentials do not match our records.',
        ];
    }
}
