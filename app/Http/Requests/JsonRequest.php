<?php

namespace App\Http\Requests;

use Pearl\RequestValidate\RequestAbstract;

/**
 * Class JsonRequest
 *
 * @author Davyd Holovii <mirage.present@gmail.com>
 * @since  30.05.2019
 */
class JsonRequest extends RequestAbstract
{
    /**
     * Validate JSON requests
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->ajax() && $this->isJson();
    }
}
