<?php

namespace App\Http\Requests;

/**
 * Class ProductRequest
 *
 * @author Davyd Holovii <mirage.present@gmail.com>
 * @since  30.05.2019
 *
 * @property-read string $name
 * @property-read float  $price
 * @property-read int    $amount
 */
class ProductRequest extends JsonRequest
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:255',
            'price' => 'required|numeric|min:0',
            'amount' => 'required|numeric|min:0',
        ];
    }
}
